#include "ToTAna.h"

void ToTAna::init(const TBCore* core)
{
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;

		int nCols = dut->getNcols();
		int nRows = dut->getNrows();

		ToTAna::nhits[iden] = 0;
		ToTAna::meanToT[iden] = 0.0;

		ToTAna::h_ToTHeatMap[iden] = new TH2D("", "ToT Heat Map;Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
		ToTAna::h_ToTHeatNormMap[iden] = new TH2D("", "ToT Heat Map (norm.);Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
		ToTAna::h_ToTSigmaMap[iden] = new TH2D("", "Sigma of ToT;Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
		ToTAna::h_ToTDiffMap[iden] = new TH2D("", "Difference of mean pixel ToT to mean sensor ToT in sigma;Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);

		// initialise vector
		ToTAna::hitToT[iden].resize(nCols*nRows);
	}	
}

void ToTAna::event(const TBCore* core, const TBEvent* event)
{
	DUT* dut = event->dut;
	int iden = event->iden;

	int nRows = dut->getNrows();
	
	// require that we have hits
	if(event->fHits != kGood)
	{
		return;
	}

	// loop over all hits
	for(auto hit: event->hits)
	{
		ToTAna::h_ToTHeatMap[iden]->Fill(hit->col, hit->row, hit->tot);
		ToTAna::meanToT[iden] += hit->tot;

		// store all ToT values
		ToTAna::hitToT[iden].at(hit->col* nRows + hit->row).push_back(hit->tot);

		ToTAna::nhits[iden]++;
	}
}

void ToTAna::finalize(const TBCore* core)
{
	core->output->processName = ToTAna::name;

	// reset output comment
	core->output->cuts = "";

	double pixelmeanToT;
	double sigma;
	
	char* histoTitle = new char[500];
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
  
		int nCols = dut->getNcols();
		int nRows = dut->getNrows();

		// calculate sensor mean ToT
		ToTAna::meanToT[iden] /= ToTAna::nhits[iden];
		TBLOG(kINFO, "DUT " << iden << " mean sensor ToT:	" << ToTAna::meanToT[iden]);

		std::sprintf(histoTitle, "ToT Heat Map DUT %i", iden);
		ToTAna::h_ToTHeatMap[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "ToT_heat_map_dut_%i", iden);
		ToTAna::h_ToTHeatMap[iden]->SetName(histoTitle);
		core->output->drawAndSave(ToTAna::h_ToTHeatMap[iden], "colz", "e");

		// loop over all pixels
		for(int ybin = 0; ybin < nRows; ybin++)
		{
			for(int xbin = 0; xbin < nCols; xbin++)
			{
				pixelmeanToT = 0;
				sigma = 0;

				// check if pixel had a hit (to avoid "0/0")
				if (not ToTAna::hitToT[iden].at(xbin* nRows + ybin).empty())
				{
					// calculate mean for every pixel
					for (auto ToT: ToTAna::hitToT[iden].at(xbin* nRows + ybin))
					{
						pixelmeanToT += ToT;
					}

					pixelmeanToT /= ToTAna::hitToT[iden].at(xbin* nRows + ybin).size();

					// calculate sigma of ToT for every pixel
					for (auto ToT: ToTAna::hitToT[iden].at(xbin* nRows + ybin))
					{
						sigma += TMath::Power(pixelmeanToT - ToT, 2);
					}

					sigma /= ToTAna::hitToT[iden].at(xbin* nRows + ybin).size() - 1;
					sigma = TMath::Sqrt(sigma);
				}

				ToTAna::h_ToTHeatNormMap[iden]->Fill(xbin, ybin, pixelmeanToT);
				ToTAna::h_ToTSigmaMap[iden]->Fill(xbin, ybin, sigma);
				ToTAna::h_ToTDiffMap[iden]->Fill(xbin, ybin, (pixelmeanToT - ToTAna::meanToT[iden])/sigma);

				ToTAna::hitToT[iden].at(xbin* nRows + ybin).clear();
			}
		}

		std::sprintf(histoTitle, "ToT Heat Norm Map DUT %i", iden);
		ToTAna::h_ToTHeatNormMap[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "ToT_heat_norm_map_dut_%i", iden);
		ToTAna::h_ToTHeatNormMap[iden]->SetName(histoTitle);
		core->output->drawAndSave(ToTAna::h_ToTHeatNormMap[iden], "colz", "e");

		std::sprintf(histoTitle, "ToT sigma DUT %i", iden);
		ToTAna::h_ToTSigmaMap[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "ToT_sigma_dut_%i", iden);
		ToTAna::h_ToTSigmaMap[iden]->SetName(histoTitle);
		core->output->drawAndSave(ToTAna::h_ToTSigmaMap[iden], "colz", "e");

		std::sprintf(histoTitle, "Difference of mean pixel ToT to mean sensor ToT in sigma DUT %i", iden);
		ToTAna::h_ToTDiffMap[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "ToT_diff_dut_%i", iden);
		ToTAna::h_ToTDiffMap[iden]->SetName(histoTitle);
		core->output->drawAndSave(ToTAna::h_ToTDiffMap[iden], "colz", "e");

		ToTAna::hitToT[iden].clear();
	}

	ToTAna::h_ToTHeatMap.clear();
	ToTAna::h_ToTHeatNormMap.clear();
	ToTAna::h_ToTSigmaMap.clear();
	ToTAna::h_ToTDiffMap.clear();

	ToTAna::hitToT.clear();

	ToTAna::meanToT.clear();
	ToTAna::nhits.clear();

	delete[] histoTitle;
}
