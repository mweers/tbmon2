/* 
 * File:   ClusterVsRun.cc
 * Author: daniel
 * 
 * Created on 24. Februar 2014, 21:09
 */

#include "ClustersVsRun.h"

void ClustersVsRun::init(const TBCore* core)
{
	TBLOG(kDEBUG3, "Initializing ClustersVsRun processor.");
	
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
	
		std::string param = core->getParam(iden, ClustersVsRun::name, "ClusterMin");
		if(param.compare("") != 0 )
		{
			ClustersVsRun::ClusterMin[iden] = std::stod(param);
		}
		else // default value
		{
			ClustersVsRun::ClusterMin[iden] = 0.5;
		}
		TBLOG(kDEBUG3, "ClusterMin=" << ClustersVsRun::ClusterMin[iden] << " has been set.");

		param = core->getParam(iden, ClustersVsRun::name, "ClusterMax");
		if(param.compare("") != 0)
		{
			ClustersVsRun::ClusterMax[iden] = std::stod(param);
		}
		else // default value
		{
			ClustersVsRun::ClusterMax[iden] = 1.5;
		}
		TBLOG(kDEBUG3, "ClusterMax=" << ClustersVsRun::ClusterMax[iden] << " has been set.");
	
		
		for(auto currentRun: core->tbconfig->rawDataRuns)
		{

			ClustersVsRun::nclusters[iden][currentRun] = 0;
			ClustersVsRun::sumclusters[iden][currentRun] = 0;
			ClustersVsRun::sumclusters_x[iden][currentRun] = 0;
			ClustersVsRun::sumclusters_y[iden][currentRun] = 0;
			TBLOG(kDEBUG3, "Initialized storage space for run " << (int)(currentRun) << ".");
		}
	}
	
	if(core->tbconfig->rawDataRuns.size() < 2)
	{
		TBLOG(kERROR, "Does not work properly with less than two runs.");
	}
	TBLOG(kDEBUG3, "Initialization of ClustersVsRun processor has been finished.");
}

void ClustersVsRun::event(const TBCore* core, const TBEvent* event)
{
	DUT* dut = event->dut;
	int iden = event->iden;
	int currentRun = core->currentRun;
	
	if(event->fTracks != kGood)
	{
		return;
	}
	
	if(event->fClusters != kGood)
	{
		return;
	}
	
	for(auto tbtrack: event->tracks)
	{
		if(tbtrack->fTrackMaskedRegion == kGood)
		{
			continue;
		}
		
		if(tbtrack->fMatchedCluster == kBad)
		{
			continue;
		}
		
		int clusterSize = tbtrack->matchedCluster->hits.size();
		ClustersVsRun::nclusters[iden][currentRun]++;
		ClustersVsRun::sumclusters[iden][currentRun] += clusterSize;
		ClustersVsRun::sumclusters_x[iden][currentRun] += TBCluster::spannedCols(tbtrack->matchedCluster, event);
		ClustersVsRun::sumclusters_y[iden][currentRun] += TBCluster::spannedRows(tbtrack->matchedCluster, event);
	}
}

void ClustersVsRun::finalize(const TBCore* core)
{
	core->output->processName = ClustersVsRun::name;
	
	char* histoTitle = new char[500];
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
		
		// set up cuts
		core->output->cuts = "good matched tracks";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		int nRuns = ClustersVsRun::nclusters[iden].size();

		double* runs = new double[nRuns];
		double* clustermatched = new double[nRuns];
		double* clustermatched_x = new double[nRuns];
		double* clustermatched_y = new double[nRuns];

		int minRunNum = core->tbconfig->rawDataRuns.at(0);
		int maxRunNum = core->tbconfig->rawDataRuns.at(nRuns-1);
		
		ClustersVsRun::histo_matchClusterSizeVsRun[iden] = new TH1D("",
										   ";Run Number;(Matched) Cluster Size",
										  maxRunNum-minRunNum, minRunNum, maxRunNum);
		ClustersVsRun::histo_matchClusterSizeXVsRun[iden] = new TH1D("",
											";Run Number;(Matched) Cluster Size X",
											maxRunNum-minRunNum, minRunNum, maxRunNum);
		ClustersVsRun::histo_matchClusterSizeYVsRun[iden] = new TH1D("",
											";Run Number;(Matched) Cluster Size Y",
											maxRunNum-minRunNum, minRunNum, maxRunNum);
	
	

		for (int irun=0; irun<nRuns; irun++)
		{
			int run = core->tbconfig->rawDataRuns.at(irun);
			runs[irun] = (double) run;

			if(ClustersVsRun::nclusters[iden][run] != 0 && ClustersVsRun::sumclusters[iden][run] != 0)
			{
				clustermatched[irun] = (double) ClustersVsRun::sumclusters[iden][run]/ (double) ClustersVsRun::nclusters[dut->iden][run];			// Save mean of matched cluster size
				clustermatched_x[irun] = (double) ClustersVsRun::sumclusters_x[iden][run]/ (double) ClustersVsRun::nclusters[iden][run];		// Save mean of matched cluster size in x direction
				clustermatched_y[irun] = (double) ClustersVsRun::sumclusters_y[iden][run]/ (double) ClustersVsRun::nclusters[dut->iden][run];		// Save mean of matched cluster size in y direction


				if(ClustersVsRun::ClusterMin[dut->iden] > clustermatched[irun])
				{
					ClustersVsRun::ClusterMax[dut->iden] = clustermatched[irun]-1; // Change plot axis max
				}
				if(ClustersVsRun::ClusterMax[dut->iden] < clustermatched[irun])
				{
					ClustersVsRun::ClusterMax[dut->iden] = clustermatched[irun]+1;	// Change plot axis min
				}

			}
			else
			{
				clustermatched[irun] = 0;
			}
			
			ClustersVsRun::histo_matchClusterSizeVsRun[iden]->SetBinContent(run-minRunNum+1, clustermatched[irun]);
			ClustersVsRun::histo_matchClusterSizeXVsRun[iden]->SetBinContent(run-minRunNum+1, clustermatched_x[irun]);
			ClustersVsRun::histo_matchClusterSizeYVsRun[iden]->SetBinContent(run-minRunNum+1, clustermatched_y[irun]);
		
		}
		
		// Save histograms to root file and to folder
		std::sprintf(histoTitle, "Matched Cluster Size Vs Run DUT %i", iden);
		ClustersVsRun::histo_matchClusterSizeVsRun[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterSizeVsRun_dut_%i", iden);
		ClustersVsRun::histo_matchClusterSizeVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClustersVsRun::histo_matchClusterSizeVsRun[iden], "", "m");
		
		std::sprintf(histoTitle, "Matched Cluster X Size Vs Run DUT %i", iden);
		ClustersVsRun::histo_matchClusterSizeXVsRun[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterSizeXVsRun_dut_%i", iden);
		ClustersVsRun::histo_matchClusterSizeXVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClustersVsRun::histo_matchClusterSizeXVsRun[iden], "", "m");
		
		std::sprintf(histoTitle, "Matched Cluster Y Size Vs Run DUT %i", iden);
		ClustersVsRun::histo_matchClusterSizeYVsRun[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterSizeYVsRun_dut_%i", iden);
		ClustersVsRun::histo_matchClusterSizeYVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClustersVsRun::histo_matchClusterSizeYVsRun[iden], "", "m");
		
		
		ClustersVsRun::h_matchClusterSizeVsRun[iden] = new TGraphErrors(nRuns, runs, clustermatched, 0, 0);
		ClustersVsRun::h_matchClusterSizeXVsRun[iden] = new TGraphErrors(nRuns, runs, clustermatched_x, 0, 0);
		ClustersVsRun::h_matchClusterSizeYVsRun[iden] = new TGraphErrors(nRuns, runs, clustermatched_y, 0, 0);
		
		std::sprintf(histoTitle, "Matched Cluster Size Vs Run DUT %i", iden);
		ClustersVsRun::h_matchClusterSizeVsRun[iden]->SetTitle(histoTitle);
		ClustersVsRun::h_matchClusterSizeVsRun[iden]->GetXaxis()->SetTitle("Run number");
		ClustersVsRun::h_matchClusterSizeVsRun[iden]->GetYaxis()->SetTitle("Matched Cluster Size");
		ClustersVsRun::h_matchClusterSizeVsRun[iden]->GetYaxis()->SetRangeUser(ClustersVsRun::ClusterMin[iden], ClustersVsRun::ClusterMax[iden]);
		std::sprintf(histoTitle, "matchClusterSizeVsRunGraphError_dut_%i", iden);
		ClustersVsRun::h_matchClusterSizeVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClustersVsRun::h_matchClusterSizeVsRun[iden], "AP", "");
		
		std::sprintf(histoTitle, "Matched Cluster X Size Vs Run DUT %i", iden);
		ClustersVsRun::h_matchClusterSizeXVsRun[iden]->SetTitle(histoTitle);
		ClustersVsRun::h_matchClusterSizeXVsRun[iden]->GetXaxis()->SetTitle("Run number");
		ClustersVsRun::h_matchClusterSizeXVsRun[iden]->GetYaxis()->SetTitle("Matched Cluster Size");
		ClustersVsRun::h_matchClusterSizeXVsRun[iden]->GetYaxis()->SetRangeUser(ClustersVsRun::ClusterMin[iden], ClustersVsRun::ClusterMax[iden]);
		std::sprintf(histoTitle, "matchClusterSizeXVsRunGraphError_dut_%i", iden);
		ClustersVsRun::h_matchClusterSizeXVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClustersVsRun::h_matchClusterSizeXVsRun[iden], "AP", "");
		
		std::sprintf(histoTitle, "Matched Cluster Y Size Vs Run DUT %i", iden);
		ClustersVsRun::h_matchClusterSizeYVsRun[iden]->SetTitle(histoTitle);
		ClustersVsRun::h_matchClusterSizeYVsRun[iden]->GetXaxis()->SetTitle("Run number");
		ClustersVsRun::h_matchClusterSizeYVsRun[iden]->GetYaxis()->SetTitle("Matched Cluster Size");
		ClustersVsRun::h_matchClusterSizeYVsRun[iden]->GetYaxis()->SetRangeUser(ClustersVsRun::ClusterMin[iden], ClustersVsRun::ClusterMax[iden]);
		std::sprintf(histoTitle, "matchClusterSizeYVsRunGraphError_dut_%i", iden);
		ClustersVsRun::h_matchClusterSizeYVsRun[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClustersVsRun::h_matchClusterSizeYVsRun[iden], "AP", "");
		
		delete [] runs;
		delete [] clustermatched;
		delete [] clustermatched_x;
		delete [] clustermatched_y;
		delete ClustersVsRun::histo_matchClusterSizeVsRun[iden];
		delete ClustersVsRun::histo_matchClusterSizeXVsRun[iden];
		delete ClustersVsRun::histo_matchClusterSizeYVsRun[iden];
		delete ClustersVsRun::h_matchClusterSizeVsRun[iden];
		delete ClustersVsRun::h_matchClusterSizeXVsRun[iden];
		delete ClustersVsRun::h_matchClusterSizeYVsRun[iden];
	}
	ClustersVsRun::histo_matchClusterSizeVsRun.clear();
	ClustersVsRun::histo_matchClusterSizeXVsRun.clear();
	ClustersVsRun::histo_matchClusterSizeYVsRun.clear();
	ClustersVsRun::h_matchClusterSizeVsRun.clear();
	ClustersVsRun::h_matchClusterSizeXVsRun.clear();
	ClustersVsRun::h_matchClusterSizeYVsRun.clear();
	delete [] histoTitle;
}