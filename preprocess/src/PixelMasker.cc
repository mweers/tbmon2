/* 
 * File:   PixelMasker.cc
 * Author: daniel
 * 
 * Created on 13. März 2014, 15:31
 */

#include "PixelMasker.h"

void PixelMasker::init(TBCore* core)
{
	std::string calibpath;

	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
		
		std::string param = core->getParam(iden, PixelMasker::name, "maskFilePath");
		if(param.compare("") != 0)
		{
			calibpath = param;
		}
		else // default Value
		{
			calibpath = "";
		}
		
		if (calibpath.compare("") == 0)
		{
			continue;
		}
		else
		{
			std::string fullPath = core->tbconfig->outputPath+core->tbconfig->extMaskPath+calibpath;
			std::ifstream fileStream;
			fileStream.open(fullPath);
			
			if(not fileStream.is_open())
			{
				TBLOG(kERROR, "Unable to open file " << fullPath << " for DUT " << iden << " continue without add extern mask.");
				continue;
			}
			
			int col;
			int row;
			
			while(!fileStream.eof())
			{
				fileStream >> col;
				fileStream >> row;
				
				if(fileStream.eof())
				{
					break;
				}
				
				if(col < 0 or col > dut->getNcols()-1)
				{
					continue;
				}
				
				if(row < 0 or row > dut->getNrows()-1)
				{
					continue;
				}
				
				if(dut->getMask(col, row) >= 16)
				{
					continue;
				}
				
				dut->addMask(col, row, 16); // mask because a calib file
			}
			
			fileStream.close();
			
			TBLOG(kINFO, "Add mask to pixels set in \"" << calibpath << "\" without double mask for DUT " << iden << ".");
		}
		
	}
}
